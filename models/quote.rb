require 'date'

Quote = Struct.new('Quote', :_symbol, :_open, :_previous_close, :_price, :_ask, :_bid, :_change_from_prior, :_change_from_prior_direction, :_timestamp) do
  def ask_price
    _ask.to_f
  end

  def bid_price
    _bid.to_f
  end

  def change_from_prior
    _change_from_prior.to_f
  end

  def change_from_prior_direction
    _change_from_prior_direction
  end

  def day_change_sign
    previous_close <= price ? '+' : '-'
  end

  def open
    _open.to_f
  end

  def previous_close
    _previous_close.to_f
  end

  def price
    _price.to_f
  end

  def symbol
    _symbol.upcase
  end

  def timestamp
    DateTime.parse(_timestamp)
  end

  def valid?
    !invalid?
  end

  def invalid?
    symbol.nil? || symbol.empty? || _price.nil? || _price.empty?
  end
end