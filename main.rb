require_relative 'models/brokers/broker_manager'

class Main
  attr_reader :broker_manager

  def initialize
    @broker_manager = BrokerManager.new
  end

  def self.run
    new.run
  end

  def run
    broker_manager.begin_trading
  end
end
